#!/bin/bash

#            _                _
#   ___ ___ | | ___  _ __ ___| |__
#  / __/ _ \| |/ _ \| '__/ __| '_ \
# | (_| (_) | | (_) | | _\__ \ | | |
#  \___\___/|_|\___/|_|(_)___/_| |_|

# Scott Steubing
# prints color codes.

for i in `seq 1 7 ; seq 30 48 ; seq 90 107 ` ; do 
    echo -e "\e[${i}mtest\e[0m$i" 
done
