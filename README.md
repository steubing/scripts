# Scripts

Collection of my personal scripts.

* bkup.sh - Backs up my documents, dotfiles, and configs to cloud storage.
* color.sh - Displays colors codes in their respective colors.
* esbkup.sh - Backs up my documents, dotfiles, and configs to an external hard drive.
* launchvifm.sh - Launches the vifm file manager. I have this set as my default file manager.
* macho.sh - Improved man. Need to remember where I got it. 
* secure.sh - Script from Chris Titus.
* syncbkups.sh - Synchronizes backups from one external hard drive to another

* 2020-08-24 - Updated bkup.sh
* 2020-08-25 - Adjusted rsync commands in bkup.sh
* 2020-08-25 - Updated bkup.sh
* 2020-08-31 - Updated bkup.sh
