#!/bin/bash
#                       _     _                          _
#  ___ _   _ _ __   ___| |__ | | ___   _ _ __  ___   ___| |__
# / __| | | | '_ \ / __| '_ \| |/ / | | | '_ \/ __| / __| '_ \
# \__ \ |_| | | | | (__| |_) |   <| |_| | |_) \__ \_\__ \ | | |
# |___/\__, |_| |_|\___|_.__/|_|\_\\__,_| .__/|___(_)___/_| |_|
#      |___/                            |_|

# Scott Steubing
# sychronizes backups from one external hard drive to another.

echo Backing up Data ... | lolcat

echo =========================================================================== | lolcat
echo Syncing Comics... | lolcat
rsync -av --delete /run/media/scotts/easystore/Scott/Comics /run/media/scotts/My\ Passport/Scott

echo =========================================================================== | lolcat
echo Syncing Downloads... | lolcat
rsync -av --delete /run/media/scotts/easystore/Scott/Downloads /run/media/scotts/My\ Passport/Scott

echo =========================================================================== | lolcat
echo Syncing Dropbox... | lolcat
rsync -av --delete /run/media/scotts/easystore/Scott/Dropbox /run/media/scotts/My\ Passport/Scott

echo =========================================================================== | lolcat
echo Syncing Mega... | lolcat
rsync -av --delete /run/media/scotts/easystore/Scott/MEGA /run/media/scotts/My\ Passport/Scott

echo =========================================================================== | lolcat
echo Syncing Movies... | lolcat
rsync -av --delete /run/media/scotts/easystore/Scott/Movies /run/media/scotts/My\ Passport/Scott

echo =========================================================================== | lolcat
echo Syncing Music... | lolcat
rsync -av --delete /run/media/scotts/easystore/Scott/Music /run/media/scotts/My\ Passport/Scott

#echo =========================================================================== | lolcat
#echo Syncing pCloudDrive... | lolcat
#rsync -av --delete /run/media/scotts/easystore/Scott/pCloudDrive /run/media/scotts/My\ Passport/Scott

echo =========================================================================== | lolcat
echo Syncing Pictures... | lolcat
rsync -av --delete /run/media/scotts/easystore/Scott/Pictures /run/media/scotts/My\ Passport/Scott

echo =========================================================================== | lolcat
echo Syncing TV... | lolcat
rsync -av --delete /run/media/scotts/easystore/Scott/TV /run/media/scotts/My\ Passport/Scott

echo =========================================================================== | lolcat
cowsay ...all done! | lolcat
sync

