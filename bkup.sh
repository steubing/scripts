#!/bin/bash
#  _     _                     _
# | |__ | | ___   _ _ __   ___| |__
# | '_ \| |/ / | | | '_ \ / __| '_ \
# | |_) |   <| |_| | |_) |\__ \ | | |
# |_.__/|_|\_\\__,_| .__(_)___/_| |_|
#                  |_|

# Scott Steubing
# backs up .config, Documents, etc., to cloud storage and backup drive.

echo Backing up stuff ... | lolcat

echo =========================================================================== | lolcat
echo Backing up config to MEGA... | lolcat
rsync -av --delete ~/.config/ ~/MEGA/BKUPS/config/
echo Backing up config to Mirror... | lolcat
rsync -av --delete ~/.config/ ~/Storage/Mirror/config/

echo =========================================================================== | lolcat
echo Backing up Documents to Dropbox... | lolcat
rsync -av --delete ~/Documents ~/Dropbox
echo Backing up Documents to MEGA... | lolcat
rsync -av --delete ~/Documents ~/MEGA/BKUPS
echo Backing up Documents to Mirror... | lolcat
rsync -av --delete ~/Documents ~/Storage/Mirror
#echo Backing up Documents to Google Drive... | lolcat
#rclone sync ~/Documents Gdrive:Backups

echo =========================================================================== | lolcat
echo Backing up Downloads to Mirror... | lolcat
rsync -av --delete ~/Downloads ~/Storage/Mirror

#echo ===========================================================================
#echo Backup un Driftmoon data...
#rsync -av --delete ~/.config/unity3d/Instant\ Kingdom/Driftmoon/saves ~/Storage/Cloud/Dropbox/GameData/Driftmoon

#echo =========================================================================== | lolcat
#echo Backing up Pillers of Eternity data to MEGA... | lolcat
#rsync -av --delete ~/.local/share/PillarsOfEternity/SavedGames ~/MEGA/BKUPS/GameData/PoE
#echo Backing up Pillers of Eternity data pCloud... | lolcat
#rsync -av --delete ~/.local/share/PillarsOfEternity/SavedGames ~/pCloudDrive/Backups/GameData/PoE

echo =========================================================================== | lolcat
echo Backing up Pillers of Eternity II: Deadfire data to MEGA... | lolcat
rsync -av --delete ~/.local/share/PillarsOfEternityII/SavedGames ~/MEGA/BKUPS/GameData/PoE2
#echo Backing up Pillers of Eternity II: Deadfire data to pCloud... | lolcat
#rsync -av --delete ~/.local/share/PillarsOfEternityII/SavedGames ~/pCloudDrive/Backups/GameData/PoE2

#echo ===========================================================================
#echo Backing up Torment: Tides of Numenera data...
#rsync -av --delete ~/.config/unity3d/InXile\ Entertainment/Torment/Saves ~/Storage/Cloud/Dropbox/GameData/Torment

#echo =========================================================================== | lolcat
#echo Backing up Tyranny  data to MEGA... | lolcat
#rsync -av --delete ~/.local/share/Tyranny/SavedGames ~/MEGA/BKUPS/GameData/Tyranny
#echo Backing up Tyranny  data to pCloud... | lolcat
#rsync -av --delete ~/.local/share/Tyranny/SavedGames ~/pCloudDrive/Backups/GameData/Tyranny

echo =========================================================================== | lolcat
echo Backing up Vimwiki to Mirror... | lolcat
rsync -av --delete ~/Dropbox/vimwiki ~/Storage/Mirror

echo =========================================================================== | lolcat
cowsay Backups finished. | lolcat

