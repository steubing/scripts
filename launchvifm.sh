#!/bin/bash
#  _                        _          _  __                 _
# | | __ _ _   _ _ __   ___| |____   _(_)/ _|_ __ ___    ___| |__
# | |/ _` | | | | '_ \ / __| '_ \ \ / / | |_| '_ ` _ \  / __| '_ \
# | | (_| | |_| | | | | (__| | | \ V /| |  _| | | | | |_\__ \ | | |
# |_|\__,_|\__,_|_| |_|\___|_| |_|\_/ |_|_| |_| |_| |_(_)___/_| |_|

# Scott Steubing
# script to allow vifm to be the default file manager for XFCE

xfce4-terminal -x vifm
