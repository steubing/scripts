#!/bin/bash
#            _     _                     _
#   ___  ___| |__ | | ___   _ _ __   ___| |__
#  / _ \/ __| '_ \| |/ / | | | '_ \ / __| '_ \
# |  __/\__ \ |_) |   <| |_| | |_) |\__ \ | | |
#  \___||___/_.__/|_|\_\\__,_| .__(_)___/_| |_|
#                            |_|

# Scott Steubing
# backups up Documents and Downloads to external hard drive.

echo Backing up Data ... | lolcat

rsync -av --delete ~/Documents /run/media/scotts/easystore/Scott
rsync -av --delete ~/Downloads /run/media/scotts/easystore/Scott
rsync -av --delete ~/Dropbox /run/media/scotts/easystore/Scott
rsync -av --delete ~/MEGA /run/media/scotts/easystore/Scott
#rsync -a --delete ~/pCloudDrive /run/media/scotts/easystore/Scott
echo =========================================================================== | lolcat

sync
cowsay Backups Finished. | lolcat

